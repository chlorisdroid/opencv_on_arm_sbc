# opencv_on_arm_sbc

<h1>Some simple scripts for cross-compiling opencv from git (developer version) on a powerfull hostmachine.</h1>

The opencv-binaries can be send to the arm single board computer via ssh directly without need to poweroff the sbc.
On the hostmachine we run a virtual machine which uses debian 10 netinstallation for cross compilation.

To get this running you should do all things related to cross compiling in a virtual machine.
- Install your favourrite OS into the vm and start it. Debian Buster 10 netinstall is the favourite choise here.
- download this repository into the vm
- cd into scripts folder and edit the config file to fit to your needs
- cd ..
- you have to be connected to the internet (faster=better :-)
- start the compiler pipeline with "~./fullProcess.sh"
- wait (slow inet connection=>make some cups of coffee, 5G connection=>proceed working on your sbc and ...have fun with OpenCV 4.1.1-dev)


Tested on Raspberry Pi3 B. More to come soon...especially a little HowTo get things running.
