#!/usr/bin/env bash
# Install libs
echo "----- Installing necessary packages"
sudo apt update
sudo apt install -y build-essential sshpass unzip python-pip python3-pip  cmake pkg-config libjpeg-dev libtiff5-dev libpng-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libgtk2.0-dev libgtk-3-dev libcanberra-gtk* libatlas-base-dev gfortran python2.7-dev python3-dev
pip install numpy
sudo apt install -y gcc-arm-linux-gnueabihf  g++-arm-linux-gnueabihf

# Download OpenCV
echo "----- Downloading OpenCV"

git clone --depth=1 https://github.com/opencv/opencv.git
git clone --depth=1 https://github.com/opencv/opencv_contrib.git

cd opencv
git pull
cd ../opencv_contrib

#wget -O opencv.zip https://github.com/opencv/opencv/archive/4.1.1.zip
#wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.1.1.zip
#unzip opencv.zip
#unzip opencv_contrib.zip

# yes you really need to rename the directories
#mv opencv-4.1.1/ opencv
#mv opencv_contrib-4.1.1/ opencv_contrib

