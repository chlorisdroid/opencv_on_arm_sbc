#!/usr/bin/env bash
echo "----- Creating cross-compiled build directory structure"
cd opencv
sudo rm -rf buildCross

mkdir buildCross
cd buildCross
mkdir installation


#SYSROOT=/build/root

#export PKG_CONFIG_DIR=.
#export PKG_CONFIG_LIBDIR=${SYSROOT}/usr/lib/pkgconfig:${SYSROOT}/usr/share/pkgconfig
#export PKG_CONFIG_SYSROOT_DIR=${SYSROOT}

export PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/share/pkgconfig
export PKG_CONFIG_LIBDIR=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/share/pkgconfig

make clean

PKG_CONFIG_PATH=/usr/lib/pkgconfig
echo "----- Setting up compilation"
cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_INSTALL_PREFIX=installation \
      -D CMAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_TOOLCHAIN_FILE=../platforms/linux/arm-gnueabi.toolchain.cmake \
      -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
      -D OPENCV_ENABLE_NONFREE=ON \
      -D BUILD_PERF_TESTS=OFF \
      -D BUILD_TESTS=OFF \
      -D BUILD_DOCS=OFF \
      -D WITH_JASPER=OFF \
      -D WITH_CUDA=OFF \
      -D WITH_OPENBLAS=OFF \
      -D BUILD_EXAMPLES=ON \
      -D INSTALL_PYTHON_EXAMPLES=ON \
      -D WITH_TBB=ON \
      -D WITH_HDF=OFF \
      -D WITH_OPENMP=ON \
      -D WITH_JAVA=OFF \
      -D PYTHON2_INCLUDE_PATH=/usr/include/python2.7 \
      -D PYTHON2_LIBRARIES=/usr/lib/arm-linux-gnueabihf/libpython2.7.so  \
      -D PYTHON2_NUMPY_INCLUDE_DIRS=/usr/lib/python2.7/dist-packages/numpy/core/include \
      -D PYTHON3_INCLUDE_PATH=/usr/include/python3.7m \
      -D PYTHON3_LIBRARIES=/usr/lib/arm-linux-gnueabihf/libpython3.7m.so \
      -D PYTHON3_NUMPY_INCLUDE_DIRS=/usr/lib/python3/dist-packages/numpy/core/include \
      -D BUILD_OPENCV_PYTHON2=ON \
      -D BUILD_OPENCV_PYTHON3=ON \
..

echo "----- Starting compilation"
make -j8
make install

cd installation/lib/python3.7/dist-packages/
mv cv2* cv2.so
# remove links that give an error using ldconfig on the  raspberry pi
rm ~/opencv/buildCross/installation/lib/libopencv_*.so.4.1

echo "----- OpenCV correctly built"
