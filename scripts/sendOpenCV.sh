#!/usr/bin/env bash
source scripts/config

# Send compiled OpenCV to embedded board
echo "----- Sending compiled files to board"
if [ -n "${PASSWORD}" ]; then
    sshpass -p ${PASSWORD} scp -r opencv/buildCross/installation/  ${NAME}@${ADDRESS}:~/
else
    scp -r opencv/buildCross/installation/  ${NAME}@${ADDRESS}:~/
fi

echo "----- To complete installation, log into the target board and execute these commands: "
echo "sudo rsync -av installation/ /usr/local"
echo "sudo ln -s /usr/local/lib/python2.7/dist-packages/cv2/ /usr/lib/python2.7/dist-packages/cv2"
echo "sudo ln -s /usr/local/lib/python3.7/dist-packages/cv2.so/ /usr/lib/python3/dist-packages/cv2"
echo "sudo ldconfig"
