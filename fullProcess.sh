#!/usr/bin/env bash

source scripts/config
 
chmod +x scripts/*.sh

scripts/downloadPackages.sh
scripts/importFiles.sh
scripts/crossCompile.sh
scripts/sendOpenCV.sh
 
echo "----- opencv 4.1.1 has been transferred to your single board computer now. --------"
